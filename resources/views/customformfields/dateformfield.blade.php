<date-component
    cssclass="form-control"
    name="{{ $row->field }}"
    dataname="{{ $row->display_name }}"
    @if($row->required == 1) required @endif
    placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
    fieldvalue="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@else{{old($row->field)}}@endif">
</date-component>

{{--  <date-component
       cssclass="form-control"
       name="{{ $row->field }}"
       dataname="{{ $row->display_name }}"
       @if($row->required == 1) required @endif
       step="any"
       placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
       value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@else{{old($row->field)}}@endif">
</date-component>  --}}
