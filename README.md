## Vuejs Custom Field Test Project

In this project, a date field is implemented with Vuejs and embeded into Voyager backend. In order to create a voyager custom field, I have set up the files in following folders.

### Laravel and Voyager

- Laravel 7.2
- [Voyager 1.4](https://voyager-docs.devdojo.com/getting-started/installation) with dummy data.

## Notes

- You have to always use "npm run watch" in order to avoid manual build process.
- Always refresh the browser with Ctrl + F5 or Cmd + Shift + R, to clear cache.

### Adding Custom Formfields to Voyager

- Refer to [Voyager Doc](https://voyager-docs.devdojo.com/customization/adding-custom-formfields)
- Crate a php class in "app/CustomFormFields/DateFormField.php". A php class with a function responsible to handle all the field parameters. The codename variable is used in the dropdown you see in the BREAD builder. In the createContent method we are returning the view that is used to display our form field.
- Create a view in "resources/views/customformfields/dateformfield.blade.php". This view file holds the vue component. 
- Add a line of code to "app/Providers/AppServiceProvider.php" register function for the registration of newly created custom field in Voyager. 
```php
use App\CustomFormFields\DateFormField; // at the top

Voyager::addFormField(DateFormField::class); // inside register function
```
- That's all we need in Voyager custom form field part.

### Crafting the Vue Component

- Adding vue component to voyager is a bit tricky. In this project, I have voyager backend system which shipped the backend system in JQuery along side with Vuejs.
- Since vue is already added into project by voyager, we can't traditionally add vuejs as suggested by [Laravel Article](https://laravel.com/docs/7.x/frontend). This works well only for frontned.
- First we will setup vue with "npm install". I have added a date plugin called "v-calendar" for better understanding in embeded vue plugins. 
- Create vue component file called "resources/js/components/DateComponent.vue". This file is pure vue component which can work regardless of frontend or backend.
- Register newly created vue component in "resources/js/app.js".  
```javascript
Vue.component('date-component', require('./components/DateComponent.vue').default);
```
- As mentioned above, Voyager shipped with Vue and JQuery itself, we have to disable default JQuery and Vue registration from our codes.
- Comment out the following lines from "resources/js/bootstrap.js". 
```javascript
try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}
```
- Comment out following lines from "resources/js/app.js"
```javascript
window.Vue = require('vue');
```
- Add id="app" to a top level div in "vendor/tcg/voyager/resources/views/bread/edit-add.blade.php"
- Finally, we need to include additional js into "config/voyager.php".
```php
'additional_js' => [
    //'js/custom.js',
    'js/app.js',
],
```
